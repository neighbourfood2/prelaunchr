set :deploy_user, 'neighbourfood'
set :stage, :production
set :branch, 'master'
set :rails_env, :production

set :full_app_name, "#{fetch(:application)}_#{fetch(:stage)}"
set :server_name, 'neighbourfood.mx'

server fetch(:server_name).to_s, user: fetch(:deploy_user).to_s,
                                 roles: %w(web app db), primary: true

set :tmp_dir, "/home/#{fetch(:deploy_user)}/apps/tmp"
set :deploy_to, "/home/#{fetch(:deploy_user)}/apps/#{fetch(:full_app_name)}"

set(:symlinks, [
      {
        source: 'nginx.conf',
        link: "/home/#{fetch(:deploy_user)}/nginx/#{fetch(:full_app_name)}"
      }
    ])

# Puma server stuff
set :puma_threads,    [1, 6]
set :puma_workers,    0

set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :ssh_options,     forward_agent: true, user: fetch(:deploy_user), keys: %w(~/.ssh/id_rsa.pub)
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true # Change to false when not using ActiveRecord

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end

  before :start, :make_dirs
end

namespace :deploy do
  desc 'Make sure local git is in sync with remote.'
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts 'WARNING: HEAD is not the same as origin/master'
        puts 'Run `git push` to sync changes.'
        exit
      end
    end
  end

  desc 'Uploads application.yml configuration file.'
  task :upload_app_config do
    on roles(:app) do
      execute :mkdir, '-p', "#{shared_path}/config/"
      upload!("config/deploy/app_configs/#{fetch(:stage)}.yml", "#{shared_path}/config/application.yml", via: :scp)
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      invoke 'deploy:upload_app_config'
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
      invoke 'delayed_job:restart'
    end
  end

  before :starting, :check_revision
  after :finishing,    :compile_assets
  after :finishing,    :cleanup
  after :finishing,    :restart
  after 'deploy:publishing', 'deploy:symlink:linked_files'
end
