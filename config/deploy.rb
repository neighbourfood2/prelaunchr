lock '3.4.0'

set :application, 'prelaunchr'
set :repo_url,    'git@bitbucket.org:neighbourfood2/prelaunchr.git'
set :deploy_user, 'deploy'

# Don't change these unless you know what you're doing
set :pty,         true
set :use_sudo,    false
set :deploy_via,  :remote_cache
set :deploy_to,   "/home/#{fetch(:deploy_user)}/apps/#{fetch(:application)}"

set :bundle_without, %w(development test doc).join(' ')
set :bundle_flags, '--quiet' # --deployment

## Defaults:
# set :scm,           :git
# set :branch,        :master
# set :format,        :pretty
# set :log_level,     :debug
# set :keep_releases, 5

set :linked_files, %w(config/application.yml)

set :linked_dirs, %w(log tmp vendor/bundle public/system)

set(:config_files, %w(
      nginx.conf
      application.yml
    ))
