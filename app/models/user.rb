# == Schema Information
#
# Table name: users
#
#  id            :integer          not null, primary key
#  email         :string
#  referral_code :string
#  referrer_id   :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'users_helper'

class User < ActiveRecord::Base
  belongs_to :referrer, class_name: 'User', foreign_key: 'referrer_id'
  has_many :referrals, class_name: 'User', foreign_key: 'referrer_id'

  validates :email, presence: true, uniqueness: true, format: {
    with: /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/i,
    message: 'Invalid email format.'
  }
  validates :referral_code, uniqueness: true

  before_create :create_referral_code
  after_create :send_welcome_email

  def email=(val)
    self[:email] = val.downcase
  end

  REFERRAL_STEPS = [
    {
      'count' => 5,
      'html' => '50% en tu<br>primer pedido',
      'class' => 'two',
      'image' => 'images/cream-tooltip@2x.png'
    },
    {
      'count' => 10,
      'html' => '1 comida<br>gratis',
      'class' => 'three',
      'image' => 'images/truman@2x.png'
    },
    {
      'count' => 25,
      'html' => '1 comida gratis<br>+2 boletos p/ cine',
      'class' => 'four',
      'image' => 'images/winston@2x.png'
    },
    {
      'count' => 50,
      'html' => 'Una semana<br>de comidas gratis',
      'class' => 'five',
      'image' => 'images/blade-explain@2x.png'
    }
  ].freeze

  private

  def create_referral_code
    self.referral_code = UsersHelper.unused_referral_code
  end

  def send_welcome_email
    UserMailer.delay.signup_email(self)
  end
end
