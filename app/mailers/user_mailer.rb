class UserMailer < ActionMailer::Base
  default from: "Sofi <sofia.ramirez@neighbourfood.mx>"

  def signup_email(user)
    @user = user
    @twitter_message = '#Comer es un placer. Emocionado por el lanzamiento de @neighbourfoodco.'

    mail(to: user.email, subject: '¡Gracias por registrarte!')
  end
end
