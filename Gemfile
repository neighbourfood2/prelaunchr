source 'https://rubygems.org'

ruby '2.2.2'

gem 'activeadmin', '1.0.0.pre2'
gem 'delayed_job_active_record', '~> 4.0.3'
gem 'devise'
gem 'sqlite3'
gem 'rails', '4.2.5.2'
gem 'puma'
gem 'figaro'
gem 'daemons'

gem 'execjs'
gem 'therubyracer'

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'coffee-rails', '~> 4.1.0'
  gem 'sass-rails',   '~> 5.0.1'
  gem 'uglifier'
end

group :development, :test do
  gem 'pry'
  gem 'rspec-rails', '3.4.2'
  gem 'rspec-mocks', '3.4.1'
  gem 'test-unit', '~> 3.0'
end

group :development do
  gem 'rubocop', require: false
  gem 'annotate', require: false
  gem 'railroady'
  # Automated deployment
  gem 'capistrano', '~> 3.4.0',         require: false
  gem 'capistrano-rvm',                 require: false
  gem 'capistrano-rails', '~> 1.1',     require: false
  gem 'capistrano-bundler', '~> 1.1.2', require: false
  gem 'capistrano3-puma',               require: false
  gem 'capistrano-passenger',           require: false
  gem 'capistrano-ext',                 require: false
  gem 'capistrano3-nginx', '~> 2.0',    require: false
end

group :production do
  gem 'mysql2', '~> 0.3.18'
  gem 'rails_12factor'
  gem 'rails_serve_static_assets'
end
