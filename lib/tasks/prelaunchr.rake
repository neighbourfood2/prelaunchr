require 'csv'

namespace :prelaunchr do
  desc "Will out put CSV's for each group of users you should email"
  task create_winner_csvs: :environment do
    stops = User::REFERRAL_STEPS.map { |stop| stop['count'] }

    winners = Hash.new { |h, k| h[k] = [] }
    User.all.each do |user|
      found = nil

      stops.reverse_each do |stop|
        found = stop if stop <= user.referrals.count && !found
      end

      winners[found] << user if found
    end

    winners.each do |stop, list|
      CSV.open("#{Rails.root}/lib/assets/group_#{stop}.csv", 'wb') do |csv|
        list.each do |user|
          csv << [user.email, user.referrals.count]
        end
      end
    end
  end
end
